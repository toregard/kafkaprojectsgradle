package no.techpros.orderapiv2.endpoints.rest

import no.techpros.orderapiv2.domain.Item
import no.techpros.orderapiv2.domain.Order
import no.techpros.orderapiv2.domain.OrderEvent
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime

@RestController
@RequestMapping("/api")
class OrderApi {

    @GetMapping("/order")
    fun order() : OrderRequest {

        val data = listOf<Item>(Item("1","A",2),Item("1","B",22))
        val order = Order("123456OrderId",data, LocalDateTime.now())
        val orderEvent = OrderEvent(order);
        return  OrderRequest(orderEvent)
    }

    
}

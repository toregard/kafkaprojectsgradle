package no.techpros.orderapiv2.endpoints.rest

import no.techpros.orderapiv2.domain.OrderEvent

data class OrderRequest(
        val orderEvent : OrderEvent
)

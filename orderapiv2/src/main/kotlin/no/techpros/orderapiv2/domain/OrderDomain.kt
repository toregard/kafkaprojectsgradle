package no.techpros.orderapiv2.domain

import java.time.LocalDateTime

data class OrderEvent(
        val order: no.techpros.orderapiv2.domain.Order
)

data class Order(
        val id: String,
        val items: List<Item>,
        val time: LocalDateTime
)

data class Item(
        val id: String,
        val itemName: String,
        val amount: Int
)

fun jalla (s: String ="ingen") : String = s.plus("AHLLOE")

fun String.toregard(s : String ) = jalla(s)

package no.techpros.orderapiv2

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class Orderapiv2Application

fun main(args: Array<String>) {
    runApplication<Orderapiv2Application>(*args)
}



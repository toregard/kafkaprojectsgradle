# Oversikt

### Tutorial: Reactive Spring Boot Part 1 – A Kotlin REST Service (reactive)

* https://blog.jetbrains.com/idea/2019/10/tutorial-reactive-spring-boot-a-kotlin-rest-service/

### Kafka Axion SpringBoot Examples

* https://github.com/AxonFramework/extension-kafka/tree/master/kafka-axon-example

### Creating Multi-project Builds

* https://guides.gradle.org/creating-multi-project-builds/

###  kotlin i gerneral

* https://beginnersbook.com/2019/03/kotlin-data-class/

* https://spring.io/blog/2018/05/11/new-tutorial-about-spring-boot-and-kotlin

* https://spring.io/guides/tutorials/spring-boot-kotlin/

* OrderRequest
```
{
    "orderEvent": {
        "order": {
            "id": "123456OrderId",
            "items": [
                {
                    "id": "1",
                    "itemName": "A",
                    "amount": 2
                },
                {
                    "id": "1",
                    "itemName": "A",
                    "amount": 2
                }
            ],
            "time": "2020-04-03T23:04:42.341293"
        }
    }
}
```

```
{
    "libraryeventId": null,
    "book": {
        "bookId": 1,
        "bookName": "bookName",
        "bookAuthor": "bookAuthor"
    }
}
```

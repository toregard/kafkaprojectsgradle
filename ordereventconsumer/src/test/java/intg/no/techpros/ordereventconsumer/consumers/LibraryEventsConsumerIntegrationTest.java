package no.techpros.ordereventconsumer.consumers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.techpros.ordereventconsumer.domain.Book;
import no.techpros.ordereventconsumer.domain.LibraryEvent;
import no.techpros.ordereventconsumer.domain.LibraryEventType;
import no.techpros.ordereventconsumer.jpa.LibraryEventRepository;
import no.techpros.ordereventconsumer.service.LibraryEventsService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.kafka.config.KafkaListenerEndpointRegistry;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.listener.MessageListenerContainer;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.ContainerTestUtils;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;

import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
@ActiveProfiles("test")
@EmbeddedKafka(topics = {LibraryEventsConsumerIntegrationTest.TOPIC}, partitions = 3)
@TestPropertySource(properties =
        {
                "spring.kafka.producer.bootstrap-servers=${spring.embedded.kafka.brokers}",
                "spring.kafka.consumer.bootstrap-servers=${spring.embedded.kafka.brokers}"
        }
)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class LibraryEventsConsumerIntegrationTest {
    public static final String TOPIC = "library-events";

    @Autowired
    private EmbeddedKafkaBroker embeddedKafkaBroker;

    @Autowired
    private KafkaTemplate<Integer, String> kafkaTemplate;

    @Autowired
    KafkaListenerEndpointRegistry endpointRegistry;

    @Autowired
    ObjectMapper objectMapper;

    @SpyBean
    LibraryEventsConsumer libraryEventsConsumerSpy;

    @SpyBean
    LibraryEventsService libraryEventsServiceSpy;

    @Autowired
    LibraryEventRepository libraryEventRepository;

    @BeforeEach
    void setUp() {
        for (MessageListenerContainer messageListenerContainer : endpointRegistry.getAllListenerContainers()) {
            ContainerTestUtils.waitForAssignment(messageListenerContainer, embeddedKafkaBroker.getPartitionsPerTopic());
        }
    }

    @AfterEach
    void tearDown(){
        libraryEventRepository.deleteAll();
    }

   @Test
    void publishNewLibraryEvent() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        String json = objectMapper.writeValueAsString(createLibraryevent(1,LibraryEventType.NEW));
        kafkaTemplate.sendDefault(json).get();
        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);
        //then
        verify(libraryEventsConsumerSpy, times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventsServiceSpy,times(1)).processLibraryEvent(isA(ConsumerRecord.class));
        List<LibraryEvent> libraryEventList = (List<LibraryEvent>) libraryEventRepository.findAll();
        assertThat(libraryEventList.size()).isEqualTo(1);
        assert libraryEventList.size()==1;
        libraryEventList.forEach(
                v -> {
                        assert v.getLibraryEventId()!=null;
                        assertThat(v.getBook().getBookId()).isEqualTo(1);
                        assertThat(v.getBook().getBookAuthor()).isEqualTo("at");
                        assertThat(v.getBook().getBookName()).isEqualTo("name");
                   }
        );
    }

   @Test
    void publishUpdateLibraryEvent() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
         assertThat(libraryEventRepository.findAll().iterator().hasNext()).isEqualTo(false);
         LibraryEvent libraryEvent = createLibraryevent(null, LibraryEventType.NEW);
        libraryEvent = libraryEventRepository.save(libraryEvent);

        Book updatedBook = Book.builder()
                .bookId(1).bookName("ChangeBook")
                .bookAuthor("newAuthor")
                .build();
        libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
        libraryEvent.setBook(updatedBook);
        String jsonUpdatedJson=objectMapper.writeValueAsString(libraryEvent);
        kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(),jsonUpdatedJson).get();
        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);

        //then
        LibraryEvent result =libraryEventRepository.findById(libraryEvent.getLibraryEventId()).get();
        assertThat(result.getBook().getBookName()).isEqualTo("ChangeBook");
        assertThat(result.getBook().getBookAuthor()).isEqualTo("newAuthor");
    }

    /**
     * Integration Test for "MODIFY" Library Event - Pass some random ""libraryEventId" in the Kafka Record.
     *
     * @throws JsonProcessingException
     * @throws InterruptedException
     * @throws ExecutionException
     */
    @Test
    void publishUpdateLibraryEvent_Update_Wrong_libraryEventId() throws JsonProcessingException, InterruptedException, ExecutionException {
        //given
        LibraryEvent libraryEvent = createLibraryevent(null, LibraryEventType.NEW);
        libraryEvent = libraryEventRepository.save(libraryEvent);
        //when
        libraryEvent.setLibraryEventId(2);
        libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
        String json = objectMapper.writeValueAsString(createLibraryevent(libraryEvent.getLibraryEventId(), LibraryEventType.UPDATE));
        ConsumerRecord<Integer, String> consumerRecord = new ConsumerRecord<>("topic", 0, 0, 1, json);
        //then
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    libraryEventsConsumerSpy.onMessage(consumerRecord);
                });
        assertThat(e.getMessage()).isEqualTo("Not a valid Library Event");
    }

    /**
     * Integration Test for "UPDATE" Library Event - Pass the "libraryEventId" as "null"
     * @throws JsonProcessingException
     * @throws InterruptedException
     * @throws ExecutionException
     *
     */
    @Test
    void publishUpdateLibraryEvent_Update_LibraryEventId_As_null() throws JsonProcessingException, InterruptedException, ExecutionException {
        //given
        LibraryEvent libraryEvent = createLibraryevent(null, LibraryEventType.NEW);
        libraryEvent = libraryEventRepository.save(libraryEvent);
        //when
        libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
        String json = objectMapper.writeValueAsString(createLibraryevent(null, LibraryEventType.UPDATE));
        ConsumerRecord<Integer, String>  consumerRecord= new ConsumerRecord<>("topic",0,0,1, json);
        //then
        IllegalArgumentException e =Assertions.assertThrows(IllegalArgumentException.class,
                () -> {
                    libraryEventsConsumerSpy.onMessage(consumerRecord);
                });
        assertThat(e.getMessage()).isEqualTo("Library event Id ia missing");
        verify(libraryEventsConsumerSpy, times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventsServiceSpy, times(1)).processLibraryEvent(isA(ConsumerRecord.class));
    }

    @Test
    void publishUpdateLibraryEvent_null_LibraryEventId_fasit() throws JsonProcessingException, ExecutionException, InterruptedException {
        //given
        Integer librartyEventId= 0;

        String json = objectMapper.writeValueAsString(createLibraryevent(librartyEventId, LibraryEventType.UPDATE));
        kafkaTemplate.sendDefault(librartyEventId, json).get();
        //when
        CountDownLatch latch = new CountDownLatch(1);
        latch.await(3, TimeUnit.SECONDS);
        verify(libraryEventsConsumerSpy, times(1)).onMessage(isA(ConsumerRecord.class));
        verify(libraryEventsServiceSpy, times(1)).processLibraryEvent(isA(ConsumerRecord.class));

    }

    private LibraryEvent createLibraryevent(Integer libraryEventId,LibraryEventType libraryEventType ) {
        Book book = Book.builder()
                .bookId(1)
                .bookAuthor("at")
                .bookName("name")
                .build();

        return no.techpros.ordereventconsumer.domain.LibraryEvent.builder()
                .libraryEventId(libraryEventId)
                .libraryEventType(libraryEventType)
                .book(book)
                .build();
    }



    /*
    Integration Test for "MODIFY" Library Event - Pass some random ""libraryEventId" in the Kafka Record.

    @Test
    fun publishModifyLibraryEventZerolLibraryEventId() {

        //given
        val libraryEventId = 0
        val json = "{\"libraryEventId\":$libraryEventId,\"libraryEventType\":\"UPDATE\",\"book\":{\"bookId\":456," +
                "\"bookName\":\"Kafka Using Spring Boot\",\"bookAuthor\":\"Dilip\"}}"

        kafkaTemplate.sendDefault(json).get()

        //when
        var latch = CountDownLatch(1)
        latch.await(3, TimeUnit.SECONDS)

        //then
        //verify(exactly = 4) { libraryEventsConsumerSpy.onMessage(any()) }
        verify(exactly = 4) { libraryEventsServiceSpy.processLibraryEvent(any()) }
        verify(exactly = 1) { libraryEventsServiceSpy.handleRecovery(any()) }

    }

Integration Test for "UPDATE" Library Event - Pass the "libraryEventId" as "null"

@Test
fun publishModifyLibraryEventNullLibraryEventId() {

    //given
    val libraryEventId = null
    val json = "{\"libraryEventId\":$libraryEventId,\"libraryEventType\":\"UPDATE\",\"book\":{\"bookId\":456," +
            "\"bookName\":\"Kafka Using Spring Boot\",\"bookAuthor\":\"Dilip\"}}"

    kafkaTemplate.sendDefault(json).get()

    //when
    var latch = CountDownLatch(1)
    latch.await(3, TimeUnit.SECONDS)

    //then
    verify(exactly = 1) { libraryEventsConsumerSpy.onMessage(any()) }
    verify(exactly = 1) { libraryEventsServiceSpy.processLibraryEvent(any()) }

}


@Test
void publishUpdateLibraryEvent_With_Wrong_LibraryEventId() throws JsonProcessingException, ExecutionException, InterruptedException {

    String json = " {\"libraryEventId\":null,\"libraryEventType\":\"NEW\",\"book\":{\"bookId\":456,\"bookName\":\"Kafka Using Spring Boot\",\"bookAuthor\":\"Dilip\"}}";
    LibraryEvent libraryEvent = objectMapper.readValue(json, LibraryEvent.class);
    libraryEvent.getBook().setLibraryEvent(libraryEvent);
    libraryEventsRepository.save(libraryEvent);

    Book updatedBook = Book.builder()
            .bookId(456)
            .bookAuthor("Dilip")
            .bookName("Kafka Using Spring Boot")
            .build();
    libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
    libraryEvent.setBook(updatedBook);
    // for example, change libraryEventId here
    libraryEvent.setLibraryEventId(56);
    String updatedJson = objectMapper.writeValueAsString(libraryEvent);
    kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(), updatedJson).get();


    CountDownLatch countDownLatch = new CountDownLatch(1);
    countDownLatch.await(3, TimeUnit.SECONDS);

    verify(libraryEventsConsumerSpy, times(1)).onMessage(isA(ConsumerRecord.class));
    verify(libraryEventsServiceSpy, times(1)).processLibraryEvent(isA(ConsumerRecord.class));

    Optional<LibraryEvent> libraryEventOptional = libraryEventsRepository.findById(libraryEvent.getLibraryEventId());
    assertFalse(libraryEventOptional.isPresent());


    ****************
    @Test
void publishUpdateLibraryEvent_With_Null_LibraryEventId() throws JsonProcessingException, ExecutionException, InterruptedException {

    String json = " {\"libraryEventId\":null,\"libraryEventType\":\"NEW\",\"book\":{\"bookId\":456,\"bookName\":\"Kafka Using Spring Boot\",\"bookAuthor\":\"Dilip\"}}";
    LibraryEvent libraryEvent = objectMapper.readValue(json, LibraryEvent.class);
    libraryEvent.getBook().setLibraryEvent(libraryEvent);
    libraryEventsRepository.save(libraryEvent);

    Book updatedBook = Book.builder()
            .bookId(456)
            .bookAuthor("Dilip")
            .bookName("Kafka Using Spring Boot")
            .build();
    libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
    libraryEvent.setBook(updatedBook);
    // for example, change libraryEventId
    libraryEvent.setLibraryEventId(null);

    String updatedJson = objectMapper.writeValueAsString(libraryEvent);
    kafkaTemplate.sendDefault(libraryEvent.getLibraryEventId(), updatedJson).get();


    CountDownLatch countDownLatch = new CountDownLatch(1);
    countDownLatch.await(3, TimeUnit.SECONDS);

    verify(libraryEventsConsumerSpy, times(1)).onMessage(isA(ConsumerRecord.class));
    verify(libraryEventsServiceSpy, times(1)).processLibraryEvent(isA(ConsumerRecord.class));

}


}


     */
}

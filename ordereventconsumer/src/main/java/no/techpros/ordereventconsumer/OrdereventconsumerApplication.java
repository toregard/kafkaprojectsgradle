package no.techpros.ordereventconsumer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdereventconsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(OrdereventconsumerApplication.class, args);
    }

}

package no.techpros.ordereventconsumer.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import no.techpros.ordereventconsumer.domain.LibraryEvent;
import no.techpros.ordereventconsumer.jpa.LibraryEventRepository;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class LibraryEventsService {
    LibraryEventRepository libraryEventRepository;
    ObjectMapper objectMapper;

    public LibraryEventsService(LibraryEventRepository libraryEventRepository, ObjectMapper objectMapper) {
        this.libraryEventRepository = libraryEventRepository;
        this.objectMapper = objectMapper;
    }

    public void processLibraryEvent(ConsumerRecord<Integer,String> consumerRecord) throws JsonProcessingException {
        consumerRecord.value();
        LibraryEvent libraryEvent= objectMapper.readValue(consumerRecord.value(),LibraryEvent.class);
        log.info("Objectmapper {}",libraryEvent);
        switch (libraryEvent.getLibraryEventType()) {
            case NEW:
                save(libraryEvent);
                break;
            case UPDATE:
                validate(libraryEvent);
                save(libraryEvent);
                break;
            default:
                log.info("Invalid library event type");
        }
    }

    private void validate(LibraryEvent libraryEvent)   {
        if(libraryEvent.getLibraryEventId()==null){
            throw new IllegalArgumentException("Library event Id ia missing");
        }

        Optional<LibraryEvent> libraryEventOptional = libraryEventRepository.findById(libraryEvent.getLibraryEventId());
        if(!libraryEventOptional.isPresent()){
            throw new IllegalArgumentException("Not a valid Library Event");
        }
        log.info("Validation is successful for the library Event : {}", libraryEventOptional.get());
    }

    private void save(LibraryEvent libraryEvent) {
        libraryEvent.getBook().setLibraryEvent(libraryEvent);
        libraryEventRepository.save(libraryEvent);
        log.info("Successfully Persisted the library Event {}",libraryEvent);
    }


}

package no.techpros.ordereventconsumer.jpa;

import no.techpros.ordereventconsumer.domain.LibraryEvent;
import org.springframework.data.repository.CrudRepository;

public interface LibraryEventRepository extends CrudRepository<LibraryEvent, Integer> {
}

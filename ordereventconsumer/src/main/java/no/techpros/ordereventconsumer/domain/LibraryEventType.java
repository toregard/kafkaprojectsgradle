package no.techpros.ordereventconsumer.domain;

public enum LibraryEventType {
    NEW,
    UPDATE
}

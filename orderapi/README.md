
Run jar
```
java -jar -Dserver.port=8082 build/libs/ordereventconsumer-0.0.1-SNAPSHOT.jar
```

idea environment:
```
—spring.profiles.active=local
```

gradle environment:
```
./gradlew bootRun —args='--spring.profiles.active=local'
```

##Intro to Apache Kafka with Spring
* https://www.baeldung.com/spring-kafka
  () 
  
* 7 mistakes when using Apache Kafka
https://blog.softwaremill.com/7-mistakes-when-using-apache-kafka-44358cd9cd6

* json procuser
https://codenotfound.com/spring-kafka-json-serializer-deserializer-example.html

LibraryeventId
```
{
    "libraryeventId": 1,
    "book": {
        "bookId": 1,
        "bookName": "bookName",
        "bookAuthor": "bookAuthor"
    }
}

curl -i \
-d {  "libraryeventId": 1, "book": {  "bookId": 1,  "bookName": "bookName",   "bookAuthor": "bookAuthor" } } \
-H "Content-Type: application/json" \
-X POST http://localhost:8080/api/library/v1
```


## test
https://docs.gradle.org/current/dsl/org.gradle.api.tasks.SourceSet.html

## kafkaadmin
https://www.baeldung.com/spring-kafka

## Object mapper

https://www.baeldung.com/jackson-object-mapper-tutorial

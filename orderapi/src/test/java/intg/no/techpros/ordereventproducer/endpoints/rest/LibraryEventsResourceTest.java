package no.techpros.ordereventproducer.endpoints.rest;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.techpros.ordereventproducer.domain.Book;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import no.techpros.ordereventproducer.domain.LibraryEventType;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.serialization.IntegerDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Timeout;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.*;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.test.EmbeddedKafkaBroker;
import org.springframework.kafka.test.context.EmbeddedKafka;
import org.springframework.kafka.test.utils.KafkaTestUtils;
import org.springframework.test.context.TestPropertySource;
import util.CommonUtilLibraryEvent;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EmbeddedKafka(topics = {LibraryEventsResourceTest.TOPIC}, partitions = 3)
@TestPropertySource(properties = {"kafka.bootstrap-servers=${spring.embedded.kafka.brokers}"})
//@Timeout(2)
public class LibraryEventsResourceTest {
    public static final String TOPIC = "library-events";
    @Autowired
    TestRestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmbeddedKafkaBroker embeddedKafkaBroker;

    private Consumer<Integer, String> consumer;

    @BeforeEach
    void setUp() {
        Map<String, Object> config = new HashMap<>(KafkaTestUtils.consumerProps("group1", "true", embeddedKafkaBroker));
        consumer = new DefaultKafkaConsumerFactory<>(config, new IntegerDeserializer(), new StringDeserializer()).createConsumer();
        embeddedKafkaBroker.consumeFromAllEmbeddedTopics(consumer);
    }

    @AfterEach
    void afterAll() {
        consumer.close();
    }

    @Test
    void postLibraryEvent() throws JsonProcessingException {

        Book book = Book.builder()
                .bookId(1)
                .bookAuthor("at")
                .bookName("name")
                .build();

        LibraryEvent libraryEvent = LibraryEvent.builder()
                .libraryeventId(12)
                .libraryEventType(LibraryEventType.NEW)
                .book(book)
                .build();

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        HttpEntity<LibraryEvent> request = new HttpEntity<>(libraryEvent, headers);
        //when
        ResponseEntity<LibraryEvent> responseEntity = restTemplate
                .exchange(
                        LibraryEventsResource.BASE_URL,
                        HttpMethod.POST,
                        request,
                        LibraryEvent.class
                );

        assertThat(HttpStatus.CREATED).isEqualTo(responseEntity.getStatusCode());
        ConsumerRecord<Integer, String> consumerRecord = KafkaTestUtils.getSingleRecord(consumer, LibraryEventsResourceTest.TOPIC);
        String value = consumerRecord.value();
        LibraryEvent resultToTest = objectMapper.readValue(value, LibraryEvent.class);
        assertThat(resultToTest).isEqualTo(libraryEvent);
    }

    @Test
    @Timeout(5)
    void putLibraryEvent() throws InterruptedException, JsonProcessingException {
        //given
        LibraryEvent libraryEvent =CommonUtilLibraryEvent.createLibraryevent(123,456,"Dilip","Kafka using Spring Boot");
        HttpHeaders headers = new HttpHeaders();
        headers.set("content-type", MediaType.APPLICATION_JSON.toString());
        HttpEntity<LibraryEvent> request = new HttpEntity<>(libraryEvent, headers);

        //when
        ResponseEntity<LibraryEvent> responseEntity = restTemplate.exchange(LibraryEventsResource.BASE_URL, HttpMethod.PUT, request, LibraryEvent.class);

        //then
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        ConsumerRecord<Integer, String> consumerRecord = KafkaTestUtils.getSingleRecord(consumer, "library-events");
        //Thread.sleep(3000);
        String expectedRecord = "{\"libraryEventId\":123,\"libraryEventType\":\"UPDATE\",\"book\":{\"bookId\":456,\"bookName\":\"Kafka using Spring Boot\",\"bookAuthor\":\"Dilip\"}}";
        String value = consumerRecord.value();
        LibraryEvent resultToTest = objectMapper.readValue(value, LibraryEvent.class);
        assertThat(libraryEvent.getLibraryeventId()).isEqualTo(resultToTest.getLibraryeventId());
        assertThat(libraryEvent.getBook().getBookId()).isEqualTo(resultToTest.getBook().getBookId());
        assertThat(libraryEvent.getBook().getBookAuthor()).isEqualTo(resultToTest.getBook().getBookAuthor());
        assertThat(libraryEvent.getBook().getBookName()).isEqualTo(resultToTest.getBook().getBookName());
    }

}

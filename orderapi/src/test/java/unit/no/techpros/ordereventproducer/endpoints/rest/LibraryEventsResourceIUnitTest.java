package no.techpros.ordereventproducer.endpoints.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import no.techpros.ordereventproducer.domain.Book;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import no.techpros.ordereventproducer.domain.LibraryEventType;
import no.techpros.ordereventproducer.producer.LibraryEventProducer;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(LibraryEventsResource.class)
public class LibraryEventsResourceIUnitTest {

    @Autowired
    MockMvc mockMvc;

    ObjectMapper objectMapper = new ObjectMapper();

    @MockBean
    LibraryEventProducer libraryEventProducerMock;

    @Test
    void postLibraryEventHappyTest() throws Exception {
        when(libraryEventProducerMock.sendLibraryEvent(isA(LibraryEvent.class))).thenReturn(null);
        String json = objectMapper.writeValueAsString(createLibraryevent(LibraryEventType.NEW,12, 1, "at", "name"));
        post(json);
    }

    @Test
    void putLibraryEventHappyTest() throws Exception {
        when(libraryEventProducerMock.sendLibraryEvent(isA(LibraryEvent.class))).thenReturn(null);
        String json = objectMapper.writeValueAsString(createLibraryevent(LibraryEventType.NEW, 12, 1, "at", "name"));
        put(json);
    }

    @Test
    void postLibraryEventValidateTest() throws Exception {
        String json = objectMapper.writeValueAsString(createLibraryevent(LibraryEventType.NEW,1, 1, null, "name"));
        AssertionError exception = assertThrows(
                AssertionError.class,
                () -> post(json));
        assertThat(exception.getMessage().equals("Status expected:<201> but was:<400>")).isTrue();
    }

    @Test
    void postLibraryEvent_4xx_Error_for_BookAuth_Test() throws Exception {
        String json = objectMapper.writeValueAsString(createLibraryevent(LibraryEventType.NEW,1, 1, null, "name"));
        String expectedErrorMessage = "book.bookAuthor - must not be null";
        mockMvc.perform(
                MockMvcRequestBuilders
                        .post(LibraryEventsResource.BASE_URL)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().is4xxClientError())
                .andExpect(content().string(expectedErrorMessage));
    }

    private ResultActions post(String json) throws Exception {
        return mockMvc.perform(
                MockMvcRequestBuilders
                        .post(LibraryEventsResource.BASE_URL)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isCreated());
    }

    private ResultActions put(String json) throws Exception {
        return mockMvc.perform(
                MockMvcRequestBuilders
                        .put(LibraryEventsResource.BASE_URL)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }

    private LibraryEvent createLibraryevent(LibraryEventType libraryEventType,Integer eventId, Integer bookid, String bookAuth, String bookName) {
        Book book = Book.builder()
                .bookId(bookid)
                .bookAuthor(bookAuth)
                .bookName(bookName)
                .build();

        LibraryEvent libraryEvent = LibraryEvent.builder()
                .libraryeventId(eventId)
                .libraryEventType(libraryEventType)
                .book(book)
                .build();
        return libraryEvent;
    }



}

package order.techpros.ordereventproducer.producer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import no.techpros.ordereventproducer.producer.LibraryEventProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.SettableListenableFuture;
import util.CommonUtilLibraryEvent;

import java.util.concurrent.ExecutionException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class LibraryEventProducerTest {

    @Mock
    KafkaTemplate<Integer, LibraryEvent> kafkaTemplateMock;

    @InjectMocks
    LibraryEventProducer underTest;

    ObjectMapper objectMapper = new ObjectMapper();

    @BeforeEach
    public void beforeEach() {
        underTest.setTopic("topic");
    }

    @Test
    void sendLibraryEvent_failure() throws ExecutionException, InterruptedException {
        SettableListenableFuture future = new SettableListenableFuture();
        future.setException(new RuntimeException("ERROR"));
        when(kafkaTemplateMock.send(isA(ProducerRecord.class))).thenReturn(future);

        Exception e = assertThrows(
                Exception.class,
                () -> underTest
                        .sendLibraryEvent(
                                CommonUtilLibraryEvent
                                        .createLibraryevent(1, 2, "A", "B")
                        ).get()
        );
    }

    @Test
    void sendLibraryEvent_success() throws JsonProcessingException {
        SettableListenableFuture future = new SettableListenableFuture();
        LibraryEvent libraryEvent = CommonUtilLibraryEvent.createLibraryevent(1, 2, "A", "B");
        String record = objectMapper.writeValueAsString(libraryEvent);
        ProducerRecord producerRecord = new ProducerRecord("topic",libraryEvent.getLibraryeventId(),record);
        RecordMetadata recordMetadata = new RecordMetadata(
          new TopicPartition("topic",1),
           1,
                1,
                342,
                System.currentTimeMillis(),
                1,
                2);

        SendResult<Integer,String> sendResult = new SendResult<>(producerRecord,recordMetadata);
        when(kafkaTemplateMock.send(isA(ProducerRecord.class))).thenReturn(future);
        ListenableFuture<SendResult<Integer,String>> listenableFuture = underTest.sendLibraryEvent(libraryEvent);

    }
}

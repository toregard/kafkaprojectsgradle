package util;

import no.techpros.ordereventproducer.domain.Book;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import no.techpros.ordereventproducer.domain.LibraryEventType;

public class CommonUtilLibraryEvent {
    public static LibraryEvent createLibraryevent(Integer eventId, Integer bookid, String bookAuth, String bookName) {
        Book book = Book.builder()
                .bookId(bookid)
                .bookAuthor(bookAuth)
                .bookName(bookName)
                .build();

        LibraryEvent libraryEvent = LibraryEvent.builder()
                .libraryeventId(eventId)
                .libraryEventType(LibraryEventType.NEW)
                .book(book)
                .build();
        return libraryEvent;
    }
}

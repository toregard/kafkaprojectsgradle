package no.techpros.ordereventproducer.domain;

public enum LibraryEventType {
    NEW,
    UPDATE
}

package no.techpros.ordereventproducer.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Builder
public class LibraryEvent
{
    @NotNull
    private Integer libraryeventId;
    private LibraryEventType libraryEventType;
    @NotNull
    @Valid
    private Book book;
}

package no.techpros.ordereventproducer.domain;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Item {
    private String id;
    private String itemName;
    private int amount;
}


package no.techpros.ordereventproducer.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
@Profile("local")
public class AutoCreateTopicsLocalConfig {
    @Value("${kafka.topic}")
    private String LIBRARY_EVENTS;

    @Bean
    public NewTopic libararyEvents() {
        return TopicBuilder.name(LIBRARY_EVENTS)
                .partitions(3)
                .replicas(1)
                .build();
    }

}

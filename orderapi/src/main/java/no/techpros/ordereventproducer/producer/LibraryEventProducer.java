package no.techpros.ordereventproducer.producer;

import lombok.extern.slf4j.Slf4j;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;

@Component
@Slf4j
public class LibraryEventProducer {

    private KafkaTemplate<Integer, LibraryEvent> kafkaTemplate;
    private String topic;

    public LibraryEventProducer(KafkaTemplate<Integer, LibraryEvent> kafkaTemplate) {
        this.kafkaTemplate = kafkaTemplate;
        this.topic = topic;
    }

    @Autowired
    public void setTopic(@Value("${kafka.topic}")String topic) {
        this.topic = topic;
    }

    /**
     * Variant 1 send(topic,key,value)
     *
     * @param libraryEvent
     */
    public ListenableFuture sendLibraryEvent(LibraryEvent libraryEvent) {
        //ListenableFuture<SendResult<Integer, LibraryEvent>> listenableFuture = kafkaTemplate.send(topic, libraryEvent.getLibraryeventId(), libraryEvent);
        //ListenableFuture<SendResult<Integer, LibraryEvent>> listenableFuture = kafkaTemplate.send(createProducerRecord(libraryEvent.getLibraryeventId(), libraryEvent, topic));
        ListenableFuture<SendResult<Integer, LibraryEvent>> listenableFuture = kafkaTemplate.send(createProducerRecord(libraryEvent.getLibraryeventId(), libraryEvent, topic));
        listenableFuture.addCallback(new ListenableFutureCallback<>() {
            @Override
            public void onFailure(Throwable ex) {
                handleFauilure(libraryEvent.getLibraryeventId(), libraryEvent, ex);
            }

            @Override
            public void onSuccess(SendResult<Integer, LibraryEvent> result) {
                onSuccess(libraryEvent, result);
            }

            private void onSuccess(LibraryEvent libraryEvent, SendResult<Integer, LibraryEvent> result) {
                log.info("Message sent key: {} value: {} partition: {} ", libraryEvent.getLibraryeventId(), libraryEvent.toString(), result.getRecordMetadata().partition());
            }
        });
        return listenableFuture;
    }

    /**
     * Variant 2 ProducserRecord
     *
     * @param libraryEvent
     */
    public void sendLibraryEventWithProducserRecord(LibraryEvent libraryEvent) {

    }

    /**
     * Error on headers
     * org.apache.kafka.common.errors.TimeoutException: Topic library-events not present in metadata after 60000 ms.
     *
     * @param key
     * @param value
     * @param topic
     * @return
     */
    private ProducerRecord createProducerRecord(Integer key, LibraryEvent value, String topic) {
//        List<Header> KafkaHeaders = List.of(new RecordHeader("event-source", "scanner".getBytes()));
//        return new ProducerRecord(topic, key, value, KafkaHeaders);
        return new ProducerRecord(topic, key, value);

    }

    private void handleFauilure(Integer key, LibraryEvent value, Throwable error) {
        log.error("Error sending the Library event Message and the exception is {}", error.getMessage());
        try {
            throw error;
        } catch (Throwable throwable) {
            log.error("Error in OnFailure: {}", throwable.getMessage());
        }

    }
}

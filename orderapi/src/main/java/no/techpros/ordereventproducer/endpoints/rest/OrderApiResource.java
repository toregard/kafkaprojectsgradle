package no.techpros.ordereventproducer.endpoints.rest;

import no.techpros.ordereventproducer.domain.Item;
import no.techpros.ordereventproducer.domain.Order;
import no.techpros.ordereventproducer.domain.OrderEvent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.Arrays;

@RestController
@RequestMapping("/api/order")
public class OrderApiResource {

    @GetMapping
    OrderRequest order() {
        return OrderRequest
                .builder()
                .orderEvent(
                        OrderEvent
                                .builder()
                                .order(Order
                                        .builder()
                                        .id("123456OrderId")
                                        .time(LocalDateTime.now())
                                        .items(Arrays.asList(
                                                new Item("1", "A", 2),
                                                new Item("2", "B", 3)))
                                        .build())
                                .build()
                ).build();
    }

}

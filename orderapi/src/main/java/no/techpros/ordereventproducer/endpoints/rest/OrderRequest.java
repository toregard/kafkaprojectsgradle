package no.techpros.ordereventproducer.endpoints.rest;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import no.techpros.ordereventproducer.domain.OrderEvent;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class OrderRequest {
    private OrderEvent orderEvent;
}

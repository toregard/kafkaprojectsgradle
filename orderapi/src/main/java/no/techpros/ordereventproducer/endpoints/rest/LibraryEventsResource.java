package no.techpros.ordereventproducer.endpoints.rest;

import lombok.extern.slf4j.Slf4j;
import no.techpros.ordereventproducer.domain.LibraryEvent;
import no.techpros.ordereventproducer.domain.LibraryEventType;
import no.techpros.ordereventproducer.producer.LibraryEventProducer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@Slf4j
@RequestMapping(LibraryEventsResource.BASE_URL)
public class LibraryEventsResource {
    public static final String BASE_URL = "/api/v1/library";
    private LibraryEventProducer libraryEventProducer;

    public LibraryEventsResource(LibraryEventProducer libraryEventProducer) {
        this.libraryEventProducer=libraryEventProducer;
    }

    @PostMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<LibraryEvent> postLibraryevent(@RequestBody @Valid LibraryEvent libraryEvent) {
        log.info("before sendLibraryEvent post");
        libraryEvent.setLibraryEventType(LibraryEventType.NEW);
        libraryEventProducer.sendLibraryEvent(libraryEvent);
        log.info("after sendLibraryEvent post");
        return ResponseEntity.status(HttpStatus.CREATED).body(libraryEvent);
    }

    @PutMapping(
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE
    )
    public ResponseEntity<?> putLibraryevent(@RequestBody @Valid LibraryEvent libraryEvent) {
        if(libraryEvent.getLibraryeventId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Please pass the event id");
        }
        log.info("before sendLibraryEvent put");
        libraryEvent.setLibraryEventType(LibraryEventType.UPDATE);
        libraryEventProducer.sendLibraryEvent(libraryEvent);
        log.info("after sendLibraryEvent put");
        return ResponseEntity.status(HttpStatus.OK).body(libraryEvent);
    }
}

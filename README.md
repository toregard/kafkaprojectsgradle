** Test containers for testing database
TestContainers
https://www.youtube.com/watch?v=Wpz6b8ZEgcU&feature=youtu.be&t=1454


** OrderRequest
```
{
    "orderEvent": {
        "order": {
            "id": "123456OrderId",
            "items": [
                {
                    "id": "1",
                    "itemName": "A",
                    "amount": 2
                },
                {
                    "id": "2",
                    "itemName": "B",
                    "amount": 22
                }
            ],
            "time": "2020-04-03T23:04:42.341293"
        }
    }
}
```
